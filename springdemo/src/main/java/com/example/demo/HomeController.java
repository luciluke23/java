package com.example.demo;

import org.springframework.stereotype.Controller; 
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dao.Course;

import java.util.ArrayList;
import java.util.List;

@RestController
@Controller
public class HomeController {

	@RequestMapping("")
	public String home() {
		
		return "index.jsp";
	}
	
	@RequestMapping("data")
	public List<Course> getData(){
		
		List<Course> list = new ArrayList<Course>();
		
		Course c1 = new Course();
		c1.setCid(111);
		c1.setCname("BE");
		
		Course c2 = new Course();
		c2.setCid(123);
		c2.setCname("BE");
		
		list.add(c1);
		list.add(c2);
		
		return list;
		
	}
	
}
